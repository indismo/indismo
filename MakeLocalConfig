#############################################################################
#  This file is part of the indismo software. 
#  It is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by 
#  the Free Software Foundation, either version 3 of the License, or any 
#  later version.
#  The software is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License,
#  along with the software. If not, see <http://www.gnu.org/licenses/>.
#  see http://www.gnu.org/licenses/.
#
#  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and 
#  Broeckhove J. (2015) Optimizing agent-based transmission models for  
#  infectious diseases, BMC Bioinformatics.
#
#  Copyright 2015, Willem L, Stijven S and Broeckhove J.
############################################################################
#
#        This file contains local (at you site or for you personally) 
#        configuration of the Makefile. We adopted this approach
#        because Eclipse and other IDE's do not pick up your personalized
#        environment variables.
#
#############################################################################

#============================================================================
# If you want verbose Make output, modify the MAKEFLAGS macro below.
#============================================================================
MAKEFLAGS   += --no-print-directory

#============================================================================
# If you want parallel make, otherwise comment away
#============================================================================
PARALLEL_MAKE = -j8

ifeq ($(shell hostname),hopper)
		PARALLEL_MAKE = -j20      
endif

#============================================================================
# MACRO definitions you can pass on to cmake:
#============================================================================
CMAKE_BUILD_TYPE            = Release   
INDISMO_INCLUDE_DOC		   = ON
INDISMO_VERBOSE_TESTING    = OFF

ifdef SystemRoot
    # MinGW on Windows
    CMAKE_INSTALL_PREFIX  = C:/opt/indismo_repo
else
    # Mac OSX and Unix/Linux
    CMAKE_INSTALL_PREFIX  = $(HOME)/opt/indismo_repo
    
    ifeq ($(shell uname),Darwin)
		#CC  = "clang"
		#CXX = "clang++"
    	CC = "/opt/local/bin/gcc"
		CXX = "/opt/local/bin/g++"
    endif
	ifeq ($(shell uname),Linux)
		#CC  = /opt/clang/bin/clang
		#CXX = /opt/clang/bin/clang++
		#CC  = /usr/local/bin/gcc
		#CXX = /usr/local/bin/g++
    endif
    
    ifneq ($(CC),)
		CMAKE_C_COMPILER = $(CC)
	endif
	ifneq ($(CXX),)
		CMAKE_CXX_COMPILER = $(CXX)
	endif

endif


#CMAKE_CXX_FLAGS = "-Wall -pedantic -Wextra"

#============================================================================
# Choose build target directory
#============================================================================
BUILD_DIR = ./target

#############################################################################
