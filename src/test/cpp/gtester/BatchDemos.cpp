/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Implementation of scenario test running demo cases in batch mode.
 */

#include <cmath>
#include <iostream>
#include <fstream>
#include <map>
#include <omp.h>
#include <string>
#include <gtest/gtest.h>

#include "sim/Simulator.h"

using namespace std;
using namespace indismo;
using namespace ::testing;

namespace Tests {

class BatchDemos: public TestWithParam<string>
{
public:
	/// TestCase set up.
	static void SetUpTestCase() {}

	/// Tearing down TestCase
	static void TearDownTestCase() {}

protected:
	/// Destructor has to be virtual.
	virtual ~BatchDemos() {}

	/// Set up for the test fixture
	virtual void SetUp() {}

	/// Tearing down the test fixture
	virtual void TearDown() {}

	// Data members of the test fixture
	static const string                      g_population_file;
	static const unsigned int                g_num_days;
	static const map<string, unsigned int>   g_results;
	static const unsigned int                g_rng_seed;
	static const double                      g_seeding_rate;
	static const double                      g_transmission_rate;
#ifdef _OPENMP
	static const map<string, unsigned int>   g_results_omp;
#endif
};

const string         BatchDemos::g_population_file     = "../data/nassau_synt_pop_sorted.csv";
const unsigned int   BatchDemos::g_num_days            = 100U;
const unsigned int   BatchDemos::g_rng_seed            = 2015U;
const double         BatchDemos::g_seeding_rate        = 0.00001;
const double         BatchDemos::g_transmission_rate   = 0.00003;

const map<string, unsigned int> BatchDemos::g_results {
	make_pair("flute", 124217),
	make_pair("flute_sort", 121497),

	make_pair("fred", 123250),
	make_pair("fred_sort", 120822),

	make_pair("sid", 123250),
	make_pair("sid_sort", 120822),
};


TEST_P( BatchDemos, RunOnce )
{
	const string sim_name = GetParam();
#ifdef _OPENMP
	// set nb of threads to 1 to reproduce serial execution results
	omp_set_num_threads(1);
	omp_set_schedule(omp_sched_dynamic,1);
#endif

	// Check population file
	// to perform test with the make target: use path ../data/<file name>" (default)
	// to perform test from the workspace: use path ./data/<file name>
	string population_file = g_population_file;
	std::ifstream infile(population_file);
	if(!infile.good()){
		population_file = population_file.substr(1,string::npos);
	}

	// Initialize simulator
	Simulator sim;
	const auto status = sim.Initialize(sim_name,
		population_file, g_transmission_rate, g_seeding_rate, g_rng_seed);
	// Try to initialize with adapted population file location
	ASSERT_EQ( true, status) << "BatchDemos> failed to initialize " << sim_name;

	// Run simulation
	cerr << sim_name << "  starting up ..." << endl;
	for(size_t i = 0U; i < g_num_days; i++){
		sim.RunTimeStep();
	}

	// Round up
	const unsigned int nbCases = sim.GetInfectedCount();
	ASSERT_EQ(nbCases, g_results.at(sim_name)) << "!! " + sim_name + " CHANGED !!";
	cerr << sim_name << "  finished" << endl;
}

#ifdef _OPENMP

const map<string, unsigned int> BatchDemos::g_results_omp {
	make_pair("flute", 120165),
	make_pair("flute_sort", 124775),

	make_pair("fred", 126693),
	make_pair("fred_sort", 125099),

	make_pair("sid", 126693),
	make_pair("sid_sort", 125099),
};

TEST_P( BatchDemos, RunOnce_omp )
{
	const string sim_name = GetParam();

	// set nb of threads to 4
	omp_set_num_threads(4);
	// use static scheduling to obtain repeatable results
	omp_set_schedule(omp_sched_static,1);

	// Check population file
	// to perform test with the make target: use path ../data/<file name>" (default)
	// to perform test from the workspace: use path ./data/<file name>
	string population_file = g_population_file;
	std::ifstream infile(population_file);
	if(!infile.good()){
		population_file = population_file.substr(1,string::npos);
	}

	// Initialize simulator
	Simulator sim;
	const auto status = sim.Initialize(sim_name,
		population_file, g_transmission_rate, g_seeding_rate, g_rng_seed);
	// Try to initialize with adapted population file location
	ASSERT_EQ( true, status) << "BatchDemos> failed to initialize " << sim_name;

	// Run simulation
	cerr << sim_name << "  starting up ..." << endl;
	for(size_t i = 0U; i < g_num_days; i++){
		sim.RunTimeStep();
	}

	// Round up
	const unsigned int nbCases = sim.GetInfectedCount();
	ASSERT_EQ(nbCases, g_results_omp.at(sim_name)) << "!! " + sim_name + " CHANGED !!";
	cerr << sim_name << "  finished " << endl;
}
#endif


namespace {
	string scenarios[] {
		"flute",
		"flute_sort",

		"fred",
		"fred_sort",

		"sid",
		"sid_sort"
	};
}

INSTANTIATE_TEST_CASE_P(RunBatch, BatchDemos, ValuesIn(scenarios));

} //end-of-namespace-Tests


