#ifndef SUMMARY_FILE_H_INCLUDED
#define SUMMARY_FILE_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the SummaryFile class.
 */

#include <fstream>
#include <string>

namespace indismo {
namespace output {

/**
 * Produces a file with simulation summary output.
 */
class SummaryFile
{
public:
	/// Constructor: initialize.
	SummaryFile(const std::string& file = "indimo2");

	/// Destructor: close the file stream.
	~SummaryFile();

	/// Print the given output with corresponding tag.
	void Print(const std::string& tag, const std::string& population_file, unsigned int num_days,
		unsigned int population_size, double seeding_rate, double r0, double transm_rate,
		unsigned int rng_seed, unsigned int num_cases, unsigned int run_time,
	        unsigned int total_time);

private:
	/// Generate file name and open the file stream.
	void Initialize(const std::string& file);

private:
	std::ofstream 	m_fstream;     ///< The file stream.
};

} // end namespace output
} // end namespace indismo

#endif // end of include guard
