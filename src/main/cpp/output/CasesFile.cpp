/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Implementation of the CasesFile class.
 */

#include "CasesFile.h"

#include <ctime>
#include <iostream>
#include <math.h>
#include <sstream>
#include <vector>

namespace indismo {
namespace output {

using namespace std;

CasesFile::CasesFile(const std::string& file)
{
	Initialize(file);
}

CasesFile::~CasesFile()
{
	m_fstream.close();
}

void CasesFile::Initialize(const std::string& file)
{
	const string file_name = file + "_log.csv";
	m_fstream.open(file_name.c_str());
}

void CasesFile::Print(const string& sim_tag, const vector<unsigned int>& cases)
{
	m_fstream << sim_tag;
	for (const auto& c: cases) {
		m_fstream << ";" << c;
	}
	m_fstream << endl;
}

} // end namespace output
} // end namespace indismo
