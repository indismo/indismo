/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Implementation of the SummaryFile class.
 */

#include "SummaryFile.h"

#include <ctime>
#include <iostream>
#include <sstream>
#include <omp.h>

namespace indismo {
namespace output {

using namespace std;

SummaryFile::SummaryFile(const std::string& file)
{
	Initialize(file);
}

SummaryFile::~SummaryFile()
{
	m_fstream.close();
}

void SummaryFile::Initialize(const std::string& file)
{
	const string file_name = file + "_output.csv";
	m_fstream.open(file_name.c_str());

	// add header
	m_fstream << "sim_tag;pop_file;num_days;pop_size;seeding_rate;"
	        << "R0;transm_rate;num_threads;rng_seed;run_time;total_time;num_cases;AR" << endl;
}


void SummaryFile::Print(const string& sim_tag, const std::string& population_file, unsigned int num_days,
	unsigned int population_size, double seeding_rate,  double r0, double transmission_rate,
	 unsigned int rng_seed, unsigned int num_cases, unsigned int run_time, unsigned int total_time)
{
	unsigned int num_threads = 0;

#ifdef _OPENMP
#pragma omp parallel
	{
		num_threads = omp_get_num_threads();
	}
#endif
	m_fstream
		<< sim_tag << ";" << population_file << ";" << num_days << ";"	<< population_size << ";"
		<< seeding_rate << ";" << r0 << ";" << transmission_rate << ";" << num_threads << ";"
		<< rng_seed << ";" << run_time << ";" << total_time << ";" << num_cases << ";"
		<< static_cast<double>(num_cases) / population_size << endl;
}

} // end namespace output
} // end namespace indismo

