#ifndef CONTACTHANDLER_H_INCLUDED
#define CONTACTHANDLER_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the ContactHandler class.
 */

#include "util/Random.h"

namespace indismo {

/**
 * Processes the contacts between persons and determines whether transmission occurs.
 */
class ContactHandler
{
public:
	/// Constructor sets the transmission rate and random number generator.
	ContactHandler(double transmission_rate, unsigned int seed, unsigned int stream_count, unsigned int id)
			: m_transmission_rate_adult(transmission_rate), m_rng(seed)
	{
		m_rng.Split(stream_count, id);
	}

	/// Handle one contact between persons of the given age. Performs a Bernoulli process with a random
	/// number. The given ages determine the transmission rate (=probability for "success").
	bool operator()(unsigned int age1, unsigned int age2)
	{
		return m_rng.NextDouble() < (m_transmission_rate_adult * (1 + (age1 < 18U && age2 < 18U)));
	}


	/// Handle two contacts between persons of the given age.
	bool makeTwoContacts(unsigned int age1, unsigned int age2)
	{
		// E.G. probability 1 contact: 1/4
		// - at home and in community: 1/4 * 1/4 = 1/16
		// - only at home: at home * (no community) = 1/4.(1-1/4) = 3/16
		// - only in community: (not at home) * community = (1-1/4) * 1/4 = 3/16
		// - no contact at all: (1-1/4)*(1-1/4) = 9/16
		// - at home OR community: not (no contact at all) = 1-(1-1/4)*(1-1/4) = 7/16

		const double once   = (m_transmission_rate_adult * (1.0 + (age1 < 18U && age2 < 18U)));
		const double twice  = 1.0 - ((1.0 - once) * (1.0 - once));
		return m_rng.NextDouble() < twice;
	}

private:
	double         m_transmission_rate_adult;  	//< Transmission rate between adults.
	util::Random   m_rng;				//< Random number engine.
};

} // namespace indismo

#endif // include guard
