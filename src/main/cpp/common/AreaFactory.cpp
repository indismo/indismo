/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Produces factory function for Area implementations.
 */

#include "AreaFactory.h"

#include <iostream>

#include "flute/Area.h"
#include "fred/Area.h"
#include "sid/Area.h"

namespace indismo {

using namespace std;

shared_ptr<IArea> AreaFactory::Create(const string&  name, double transmission_rate, unsigned int rng_seed)
{
	shared_ptr<IArea> area;

	if (name == "flute") {
		area = make_shared<flute::Area<plain_tag>>(transmission_rate, rng_seed);
	} else if (name == "flute_sort") {
		area = make_shared<flute::Area<sort_tag>>(transmission_rate, rng_seed);
		//
	} else if (name == "fred") {
		area = make_shared<fred::Area<plain_tag>>(transmission_rate, rng_seed);
	} else if (name == "fred_sort") {
		area = make_shared<fred::Area<sort_tag>>(transmission_rate, rng_seed);
		//
	} else if (name == "sid") {
		area = make_shared<sid::Area<plain_tag>>(transmission_rate, rng_seed);
	} else if (name == "sid_sort") {
		area = make_shared<sid::Area<sort_tag>>(transmission_rate, rng_seed);
		//
	} else{
		std::cerr << "SIMULATOR NAME UNKOWN!" << std::endl;
	}

	return area;
}

} // namespace indismo
