#ifndef IMPLEMENTATIONS_H_INCLUDED
#define IMPLEMENTATIONS_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * A collection of tags and AlgorithmTraits to differentiate between model implementations.
 */

#include <string>

#include "util/conststr.h"

namespace indismo {

/// Cluster::Update algorithm tag: do not use sorting (basic)
struct no_sort_tag {};
/// Cluster::Update algorithm tag: use sorting
struct sort_tag {};

/// algorithm tag: basic
struct plain_tag {};

/// Algorithm trait: template structure
template<typename A>
struct AlgorithmTraits;

/// Algorithm trait: basic
template<>
struct AlgorithmTraits<plain_tag>
{
	using SortType            = no_sort_tag;
};

/// Algorithm trait: use sorting
template<>
struct AlgorithmTraits<sort_tag>
{
	using SortType            = sort_tag;
};


template<typename T> inline constexpr util::conststr string_rep()               { return util::conststr(""); }

template<> inline constexpr util::conststr string_rep<plain_tag>()              { return util::conststr(""); }
template<> inline constexpr util::conststr string_rep<sort_tag>()               { return util::conststr("_sort"); }

} // namespace indismo

#endif // end of include guard
