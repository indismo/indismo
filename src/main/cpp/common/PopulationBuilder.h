#ifndef POPULATION_BUILDER_H_INCLUDED
#define POPULATION_BUILDER_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Initialize populations.
 */

#include <fstream>
#include <memory>
#include <string>

#include "util/StringUtils.h"
#include "util/Random.h"

namespace indismo {

/**
 * Initializes Population objects.
 */
template<typename P>
class PopulationBuilder
{
public:
	/**
	 * Constructor that initializes a Population: Add persons and set the first infected cases
	 *
	 * @param population		Pointer to the initialized population.
	 * @param population_file_name	The population data
	 * @param seeding_rate		The rate for seeding infected people in the population.
	 * @param rng_seed		The Random Number Generator seed
	 */
	static bool Build(
		std::shared_ptr<P> population, const std::string& population_file_name,
		double seeding_rate, unsigned int rng_seed)
	{
		bool status = false;

		//------------------------------------------------
		// Add persons to population.
		//------------------------------------------------
		std::ifstream popFile;
		popFile.open(population_file_name);
		if (popFile.is_open()) {
			std::string line;
			std::getline(popFile, line); // step over file header
			unsigned int person_index = 0U;
			while (std::getline(popFile, line)) {
				const auto values = util::StringUtils::Tokenize(line, ";");
				population->AddPerson(person_index,
					util::StringUtils::FromString<unsigned int>(values[0]),
					util::StringUtils::FromString<unsigned int>(values[1]),
					util::StringUtils::FromString<unsigned int>(values[3]),
					util::StringUtils::FromString<unsigned int>(values[2]),
					util::StringUtils::FromString<unsigned int>(values[4]));
				++person_index;
			}
			popFile.close();
			status = true;
		}

		//------------------------------------------------
		// Seed infected persons.
		//------------------------------------------------
		if (status) {
			unsigned int population_size = population->GetSize();
			unsigned int num_infected = floor(static_cast<double>(population_size) * seeding_rate);
			unsigned int max_population_index = population_size - 1;
			util::Random rng(rng_seed);
			if (num_infected > 0) {
				for (unsigned int i = 0; i < num_infected; ++i) {
					(*population).SetIndexCase(rng(max_population_index));
				}
			}
		}

		//------------------------------------------------
		// Done
		//------------------------------------------------
		return status;
	}

private:

};

} // namespace indismo

#endif // include guard
