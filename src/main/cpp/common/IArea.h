#ifndef COMMON_IAREA_H_INCLUDED
#define COMMON_IAREA_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Interface class for Area.
 */

#include <string>

namespace indismo {

/**
 * Area interface
 */
class IArea
{
public:
	/// Destructor is virtual
	virtual ~IArea() {}

	/// Get the cumulative number of cases.
	virtual unsigned int GetInfectedCount() const = 0;

	/// Get the population size.
	virtual unsigned int GetPopulationSize() const = 0;

	/// Return a tag that identifies this (I)Area
	virtual std::string GetTag() const = 0;

	/// Initialize the Area with given parameters.
	virtual bool Initialize(const std::string& population_file_name,
		double seeding_rate, unsigned int rng_seed) = 0;

	/// Process one time step.
	virtual void TimeStep() = 0;
};

} // namespace indismo

#endif // end of include guard
