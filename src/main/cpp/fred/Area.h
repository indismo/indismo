#ifndef FRED_AREA_H_INCLUDED
#define FRED_AREA_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the fred Area class
 */

#include <type_traits>
#include <memory>
#include <string>
#include <vector>

#include "common/ContactHandler.h"
#include "common/IArea.h"
#include "common/implementations.h"
#include "fred/Cluster.h"

namespace indismo {
namespace fred {

class Population;

/**
 * Geographical container for the virtual world.
 */
template<typename A>
class Area : public IArea
{
public:
	using algorithm_tag   = A;
	using SortType        = typename AlgorithmTraits<A>::SortType;

public:
	/// Constructor: initialize the transmission rate and random number generator seed.
	Area(const double transmRate, const unsigned int rngSeed);

	/// Get the cumulative number of cases.
	unsigned int GetInfectedCount() const;

	/// Get the population size.
	unsigned int GetPopulationSize() const;

	/// Return a tag that identifies the Area implementation.
	std::string GetTag() const;

	/// Initialize the Area: population & clusters.
	bool Initialize(const std::string& population_file_name,
		double seeding_rate, unsigned int rng_seed);

	/// Process one time step.
	void TimeStep();

private:
	/// Initialize the clusters.
	void InitializeClusters();

	/// Initialize the population
	bool InitializePopulation(const std::string& population_file_name,
		double seeding_rate, unsigned int rng_seed);

	/// update the contacts in the given clusters.
	void UpdateContacts(std::vector<Cluster>& clusters);

private:
	unsigned int                                   m_num_threads;     ///< The number of (OpenMP) threads.
	std::shared_ptr<Population>                    m_population;      ///< Pointer to the Population.
	std::vector<Cluster>                           m_households;	  ///< Container with households Clusters.
	std::vector<Cluster>                           m_day_clusters;	  ///< Container with day Clusters.
	std::vector<Cluster>                           m_home_districts;  ///< Container with home district Clusters.
	std::vector<Cluster>                           m_day_districts;   ///< Container with day district Clusters.
	std::vector<std::shared_ptr<ContactHandler>>   m_contact_handler; ///< Pointer to the ContactHandler.
};


//-----------------------------------------------------------------------------------
// Instantiations are taken care of in .cpp file..
//-----------------------------------------------------------------------------------

extern template class Area<plain_tag>;
extern template class Area<sort_tag>;

} // namespace fred
} // namespace indismo

#endif // end of include guard
