/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Implementation of the fred Area class.
 */

#include "fred/Area.h"

#include <omp.h>
#include "common/PopulationBuilder.h"
#include "fred/Population.h"

using namespace std;

namespace indismo {
namespace fred {

template <typename A>
Area<A>::Area(const double transmission_rate, const unsigned int rng_seed)
	: m_num_threads(1U), m_population(std::make_shared<Population>())
{
	#pragma omp parallel
	{
		#ifdef _OPENMP
		m_num_threads = omp_get_num_threads();
		#endif
	}
	for (size_t i = 0; i < m_num_threads; i++) {
		m_contact_handler.push_back(std::make_shared<ContactHandler>(transmission_rate,
			rng_seed, m_num_threads, i));
	}
}

template <typename A>
unsigned int Area<A>::GetInfectedCount() const
{
	return m_population->GetInfectedCount();
}

template <typename A>
unsigned int Area<A>::GetPopulationSize() const
{
	return m_population->GetSize();
}

template <typename A>
std::string Area<A>::GetTag() const
{
	return "fred" + string_rep<algorithm_tag>().to_string();
}

template <typename A>
bool Area<A>::Initialize(const string& population_file_name,
	double seeding_rate, unsigned int rng_seed)
{
	bool status = InitializePopulation(population_file_name, seeding_rate, rng_seed);
	InitializeClusters();
	return status;
}

template <typename A>
void Area<A>::InitializeClusters()
{
	// get number of clusters and districts
	unsigned int num_households   = 0U;
	unsigned int num_day_clusters  = 0U;
	unsigned int num_home_districts      = 0U;
	unsigned int num_day_districts       = 0U;
	Population& population      = *m_population;

	for (auto p : population) {
		if (num_households < p.GetHouseholdId()) {
			num_households = p.GetHouseholdId();
		}
		if (num_day_clusters < p.GetDayClusterId()) {
			num_day_clusters = p.GetDayClusterId();
		}
		if (num_home_districts < p.GetHomeDistrictId()) {
			num_home_districts = p.GetHomeDistrictId();
		}
		if (num_day_districts < p.GetDayDistrictId()) {
			num_day_districts = p.GetDayDistrictId();
		}
	}

	// add extra '0' nbh (=not present)
	num_households++;
	num_day_clusters++;
	num_home_districts++;
	num_day_districts++;

	m_households   = std::vector<Cluster>(num_households);
	m_day_clusters  = std::vector<Cluster>(num_day_clusters);
	m_home_districts     = std::vector<Cluster>(num_home_districts);
	m_day_districts      = std::vector<Cluster>(num_day_districts);

	for (size_t i = 0U; i < population.size(); i++) {
		if (population[i].GetHouseholdId() > 0) {
			m_households[population[i].GetHouseholdId()].AddPerson(&population[i]);
		}
		if (population[i].GetDayClusterId() > 0) {
			m_day_clusters[population[i].GetDayClusterId()].AddPerson(&population[i]);
		}
		if (population[i].GetHomeDistrictId() > 0) {
			m_home_districts[population[i].GetHomeDistrictId()].AddPerson(&population[i]);
		}
		if (population[i].GetDayDistrictId() > 0) {
			m_day_districts[population[i].GetDayDistrictId()].AddPerson(&population[i]);
		}
	}
}

template <typename A>
bool Area<A>::InitializePopulation(const string& population_file_name,
	double seeding_rate, unsigned int rng_seed)
{
	return PopulationBuilder<Population>::Build(m_population,
			population_file_name, seeding_rate, rng_seed);
}

template <typename A>
void Area<A>::TimeStep()
{
	UpdateContacts(m_households);
	UpdateContacts(m_day_clusters);
	UpdateContacts(m_home_districts);
	UpdateContacts(m_day_districts);
	for (auto& p : *m_population) {
		p.Update();
	}
}

template <typename A>
void Area<A>::UpdateContacts(std::vector<Cluster>& clusters)
{
	#pragma omp parallel
	{
		unsigned int thread_i = 0U;
		#ifdef _OPENMP
		thread_i = omp_get_thread_num();
		#endif
		#pragma omp for schedule(runtime)
		for (size_t cluster_i = 0; cluster_i < clusters.size(); cluster_i++) {
			clusters[cluster_i].Update(m_contact_handler[thread_i], SortType());
		}
	}
}


//-----------------------------------------------------------------------------------
// Instantiations for all algorithms.
//-----------------------------------------------------------------------------------

template class Area<plain_tag>;
template class Area<sort_tag>;

} // namespace fred
} // namespace indismo
