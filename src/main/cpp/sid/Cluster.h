#ifndef SID_CLUSTER_H_INCLUDED
#define SID_CLUSTER_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the sid Cluster class
 */

#include <memory>
#include <utility>
#include <vector>

#include "common/ContactHandler.h"
#include "common/implementations.h"
#include "sid/Population.h"

namespace indismo {
namespace sid {

/**
 * Represents a location for social contacts, an group of people.
 */
class Cluster
{
public:
	/// Constructor: store a link to the Population.
	Cluster(std::shared_ptr<Population> population)
		: m_population(population)
	{
	}

	/// Add the Person with the given Population index to the Cluster.
	void AddPerson(unsigned int pop_index)
	{
		m_members.push_back(pop_index);
	}

	/// Update the social contacts between the infectious and susceptible members.
	void Update(std::shared_ptr<ContactHandler> contact_handler, no_sort_tag)
	{
		for (size_t i_member = 0U; i_member < m_members.size(); i_member++) {
			if (m_population->IsInfectious(m_members[i_member])) {
				const double age1 = m_population->GetAge(m_members[i_member]);
				for (size_t i_contact = 0; i_contact < m_members.size(); i_contact++) {
					if (m_population->IsSusceptible(m_members[i_contact])) {
						assert(i_contact != i_member); // p1 can (should!) not be infectious and susceptible at the same time...
						const double age2 = m_population->GetAge(m_members[i_contact]);
						if ((*contact_handler)(age1, age2)) {
							m_population->StartInfection(m_members[i_contact]);
						}
					}
				}
			}
		}
	}

	/// Update the social contacts between the infectious and susceptible members with sorting on health status.
	void Update(std::shared_ptr<ContactHandler> contact_handler, sort_tag)
	{
		// check if the cluster has infected members and sort (order: non-susceptible, susceptible)
		size_t num_cases = 0;
		bool infectious_cases = false;

		for (size_t i_member = 0; i_member < m_members.size(); i_member++) {
			if (!m_population->IsSusceptible(m_members[i_member])) {
				if (!infectious_cases && m_population->IsInfectious(m_members[i_member])) {
					infectious_cases = true;
				}
				if (i_member > num_cases) {
					std::swap(m_members[i_member], m_members[num_cases]);
				}
				num_cases++;

			}
		}
		if (infectious_cases) {
			// match infectious (in first part of member list) with susceptible members (last part)
			for (size_t i_infected = 0; i_infected < num_cases; i_infected++) {
				if (m_population->IsInfectious(m_members[i_infected])) {
					const auto age1 = m_population->GetAge(m_members[i_infected]);
					for (size_t i_contact = num_cases; i_contact < m_members.size(); i_contact++) {
						if ((*contact_handler)(age1, m_population->GetAge(m_members[i_contact]))) {
							m_population->StartInfection(m_members[i_contact]);
						}
					}
				}
			}
		}
	}

private:
	std::vector<unsigned int>     m_members;     ///< Container with the Population indices of the members.
	std::shared_ptr<Population>   m_population;  ///< Points to the Population.
};


} // namespace sid
} // namespace indismo

#endif // include-guard
