/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Implementation of the sid Area class.
 */

#include "sid/Area.h"

#include <omp.h>
#include "common/PopulationBuilder.h"
#include "sid/Population.h"

using namespace std;

namespace indismo {
namespace sid {

template <typename A>
Area<A>::Area(const double transmission_rate, const unsigned int rng_seed)
	: m_num_threads(1U), m_population(std::make_shared<Population>())
{
	#pragma omp parallel
	{
		#ifdef _OPENMP
		m_num_threads = omp_get_num_threads();
		#endif
	}
	for (unsigned int i = 0; i < m_num_threads; i++) {
		m_contact_handler.push_back(std::make_shared<ContactHandler>(transmission_rate,
			rng_seed, m_num_threads, i));
	}
}

template <typename A>
unsigned int Area<A>::GetInfectedCount() const
{
	return m_population->GetInfectedCount();
}

template <typename A>
unsigned int Area<A>::GetPopulationSize() const
{
	return m_population->GetSize();
}

template <typename A>
std::string Area<A>::GetTag() const
{
	return "sid" + string_rep<algorithm_tag>().to_string();
}

template <typename A>
bool Area<A>::Initialize(const string& population_file_name,
	double seeding_rate, unsigned int rng_seed)
{
	bool status = InitializePopulation(population_file_name, seeding_rate, rng_seed);
	InitializeClusters();
	return status;
}

template <typename A>
void Area<A>::InitializeClusters()
{
	// get number of clusters and neighborhoods
	unsigned int num_households     = 0U;
	unsigned int num_day_clusters   = 0U;
	unsigned int num_home_districts = 0U;
	unsigned int num_day_districts  = 0U;

	for (unsigned int i = 0U; i < m_population->GetSize(); i++) {
		if (num_households < m_population->GetHouseholdId(i)) {
			num_households = m_population->GetHouseholdId(i);
		}
		if (num_day_clusters < m_population->GetDayClusterId(i)) {
			num_day_clusters = m_population->GetDayClusterId(i);
		}
		if (num_home_districts < m_population->GetHomeDistrictId(i)) {
			num_home_districts = m_population->GetHomeDistrictId(i);
		}
		if (num_day_districts < m_population->GetDayDistrictId(i)) {
			num_day_districts = m_population->GetDayDistrictId(i);
		}
	}

	// add extra '0' nbh (=not present)
	num_households++;
	num_day_clusters++;
	num_home_districts++;
	num_day_districts++;

	m_households   = std::vector<Cluster>(num_households, Cluster(m_population));
	m_day_clusters  = std::vector<Cluster>(num_day_clusters, Cluster(m_population));
	m_home_districts     = std::vector<Cluster>(num_home_districts, Cluster(m_population));
	m_day_districts      = std::vector<Cluster>(num_day_districts, Cluster(m_population));

	for (size_t i = 0U; i < m_population->GetSize(); i++) {
		if (m_population->GetHouseholdId(i) > 0) {
			m_households[m_population->GetHouseholdId(i)].AddPerson(i);
		}
		if (m_population->GetDayClusterId(i) > 0) {
			m_day_clusters[m_population->GetDayClusterId(i)].AddPerson(i);
		}
		if (m_population->GetHomeDistrictId(i) > 0) {
			m_home_districts[m_population->GetHomeDistrictId(i)].AddPerson(i);
		}
		if (m_population->GetDayDistrictId(i) > 0) {
			m_day_districts[m_population->GetDayDistrictId(i)].AddPerson(i);
		}
	}
}

template <typename A>
bool Area<A>::InitializePopulation(const string& population_file_name,
	double seeding_rate, unsigned int rng_seed)
{
	return PopulationBuilder<Population>::Build(m_population,
			population_file_name, seeding_rate, rng_seed);
}

template <typename A>
void Area<A>::TimeStep()
{
	UpdateContacts(m_households);
	UpdateContacts(m_day_clusters);
	UpdateContacts(m_home_districts);
	UpdateContacts(m_day_districts);
	for (unsigned int i = 0U; i < m_population->GetSize(); i++) {
		m_population->Update(i);
	}
}

template <typename A>
void Area<A>::UpdateContacts(std::vector<Cluster>& clusters)
{
	#pragma omp parallel
	{
		unsigned int thread_i = 0U;
		#ifdef _OPENMP
		thread_i = omp_get_thread_num();
		#endif
		#pragma omp for schedule(runtime)
		for (unsigned int cluster_i = 0; cluster_i < clusters.size(); cluster_i++) {
			clusters[cluster_i].Update(m_contact_handler[thread_i], SortType());
		}
	}
}


//-----------------------------------------------------------------------------------
// Instantiations for all algorithms.
//-----------------------------------------------------------------------------------

template class Area<plain_tag>;
template class Area<sort_tag>;

} // namespace sid
} // namespace indismo
