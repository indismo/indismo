#ifndef SID_POPULATION_H_INCLUDED
#define SID_POPULATION_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header file for the sid Population class
 */

#include <vector>

#include "util/TrackIndexCase.h"

namespace indismo {
namespace sid {

/**
 * Container for the person data.
 *
 * @note: We didn't use vector<bool> for the health status variables to prevent optimization by
 * 	 coalescing vector elements to reduce memory size. This requires extra overhead to access
 * 	 the 'bool-like' elements.
 */
class Population
{
public:
	/// Add a person with given data to the Population.
	void AddPerson(
		unsigned int id, double age, unsigned int household_id,
		unsigned int home_district_id, unsigned int day_cluster_id,
		unsigned int day_district_id)
	{
		m_ids.push_back(id);
		m_ages.push_back(age);
		m_household_ids.push_back(household_id);
		m_home_district_ids.push_back(home_district_id);
		m_day_cluster_ids.push_back(day_cluster_id);
		m_day_district_ids.push_back(day_district_id);
		m_susceptible.push_back(1U);
		m_infected.push_back(0U);
		m_infectious.push_back(0U);
		m_recovered.push_back(0U);
		m_disease_counters.push_back(0U);
	}

	/// Get the age of the person with given index.
	double GetAge(const size_t m_data_index) const
	{
		return m_ages[m_data_index];
	}

	/// Get the day cluster id of the person with given index.
	unsigned int GetDayClusterId(const size_t m_data_index) const
	{
		return m_day_cluster_ids[m_data_index];
	}

	/// Get the day district id of the person with given index.
	unsigned int GetDayDistrictId(const size_t m_data_index) const
	{
		return m_day_district_ids[m_data_index];
	}

	/// Get the household id of the person with given index.
	unsigned int GetHouseholdId(const size_t m_data_index) const
	{
		return m_household_ids[m_data_index];
	}

	/// Get the home district of the person with given index.
	unsigned int GetHomeDistrictId(const size_t m_data_index) const
	{
		return m_home_district_ids[m_data_index];
	}

	/// Get the id of the person with given index.
	unsigned int GetId(const size_t m_data_index) const
	{
		return m_ids[m_data_index];
	}

	/// Get the cumulative number of cases.
	unsigned int GetInfectedCount() const
	{
		unsigned int total = 0;
		for (unsigned int i = 0U; i < GetSize(); i++) {
			total += (IsInfected(i) || IsRecovered(i) ? 1U : 0U);
		}
		return total;
	}

	/// Get the Population size
	size_t GetSize() const {
		return m_ids.size();
	}

	/// Is the person with given index susceptible?
	bool IsSusceptible(const size_t m_data_index) const
	{
		return m_susceptible[m_data_index];
	}

	/// Is the person with given index infected?
	bool IsInfected(const size_t m_data_index) const
	{
		return m_infected[m_data_index];
	}

	/// Is the person with given index infectious?
	bool IsInfectious(const size_t m_data_index) const
	{
		return m_infectious[m_data_index];
	}

	/// Is the person with given index recovered?
	bool IsRecovered(const size_t m_data_index) const
	{
		return m_recovered[m_data_index];
	}

	/// Get the disease counter of the person with given index.
	unsigned int GetDiseaseCounter(const size_t m_data_index) const
	{
		return m_disease_counters[m_data_index];
	}

	/// Increment the disease counter of the person with given index.
	void IncrementDiseaseCounter(const size_t m_data_index)
	{
		m_disease_counters[m_data_index]++;
	}

	/// Reset the disease counter of the person with given index.
	void ResetDiseaseCounter(const size_t m_data_index)
	{
		m_disease_counters[m_data_index] = 0U;
	}

	/**
	 * Set the person with given index as index case.
	 *
	 * @note StartInfection() is not used since this method can be adapted to estimate R0
	 */
	void SetIndexCase(const size_t m_data_index)
	{
		m_susceptible[m_data_index] 	= 0U;
		m_infected[m_data_index] 	= 1U;
		ResetDiseaseCounter(m_data_index);
	}

	/**
	 * Start infection.
	 *
	 * @note To estimate R0, we need to track only the index case(s) so secondary cases are not infectious.
	 */
	void StartInfection(const size_t m_data_index)
	{
		assert(m_susceptible[m_data_index]);
		m_susceptible[m_data_index] 	= 0U;
		m_infected[m_data_index] 	= 1U;
		ResetDiseaseCounter(m_data_index);

		if(TRACK_INDEX_CASE){
			StopInfection(m_data_index);
		}
	}

	/// Stop the infection of the person with given index.
	void StopInfection(const size_t m_data_index)
	{
		m_infected[m_data_index]    = 0U;
		m_infectious[m_data_index]  = 0U;
		m_recovered[m_data_index]   = 1U;
	}

	/// Update the health status of the person with given index.
	void Update(const size_t m_data_index)
	{
		if (IsInfected(m_data_index)) {
			IncrementDiseaseCounter(m_data_index);
			if (GetDiseaseCounter(m_data_index) == 2U) {
				m_infectious[m_data_index] = 1U;
			} else if (GetDiseaseCounter(m_data_index) == 6U) {
				StopInfection(m_data_index);
			}
		}
	}

private:
	std::vector<unsigned int>    m_ids;		  ///< Container with the persons id.
	std::vector<double>          m_ages;		  ///< Container with the persons age.

	std::vector<unsigned int>    m_household_ids;	  ///< Container with the persons household id.
	std::vector<unsigned int>    m_home_district_ids; ///< Container with the persons home district id.
	std::vector<unsigned int>    m_day_cluster_ids;	  ///< Container with the persons day cluster id.
	std::vector<unsigned int>    m_day_district_ids;  ///< Container with the persons day district id.

	std::vector<unsigned int>    m_susceptible;	  ///< Container with the persons susceptible status. A vector with unsigned integers is used because a vector of booleans is compressed to reduce memory but this provides overhead for processing.
	std::vector<unsigned int>    m_infected;	  ///< Container with the persons infected status. A vector with unsigned integers is used because a vector of booleans is compressed to reduce memory but this provides overhead for processing.
	std::vector<unsigned int>    m_infectious;  	  ///< Container with the persons infectious status. A vector with unsigned integers is used because a vector of booleans is compressed to reduce memory but this provides overhead for processing.
	std::vector<unsigned int>    m_recovered;	  ///< Container with the persons recovered status. A vector with unsigned integers is used because a vector of booleans is compressed to reduce memory but this provides overhead for processing.

	std::vector<unsigned int>    m_disease_counters;  ///< Container with the persons disease counter. A vector with unsigned integers is used because a vector of booleans is compressed to reduce memory but this provides overhead for processing.
};

} // namespace sid
} // namespace indismo

#endif // end of include guard
