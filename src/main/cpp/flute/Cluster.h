#ifndef FLUTE_CLUSTER_H_INCLUDED
#define FLUTE_CLUSTER_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the flute Cluster class
 */

#include <memory>
#include <utility>
#include <vector>

#include "common/ContactHandler.h"
#include "common/implementations.h"
#include "flute/Person.h"

namespace indismo {
namespace flute {

/**
 * Represents a location for social contacts, an group of people.
 */
class Cluster
{
public:
	/// Add the given Person to the Cluster.
	void AddPerson(Person* p)
	{
		m_members.push_back(p);
	}

	/// Update the social contacts between the infectious and susceptible members.
	void Update(unsigned int(Person::*getInternalClusterId)() const, std::shared_ptr<ContactHandler> contact_handler, no_sort_tag)
	{
		for (const auto p1 : m_members) {
			if (p1->IsInfectious()) {
				const double age1 = p1->GetAge();
				const unsigned int sub_cluster1 = (p1->*getInternalClusterId)();
				for (auto p2 : m_members) {
					if (p2->IsSusceptible()) {
						assert(p2 != p1); // p1 can (should!) not be infectious and susceptible at the same time...
						const double age2 = p2->GetAge();
						const unsigned int sub_cluster2 = (p2->*getInternalClusterId)();
						// check internal cluster id's
						if (sub_cluster1 == sub_cluster2) {
							//contacts on small and large cluster level
							if (contact_handler->makeTwoContacts(age1, age2)) {
								p2->StartInfection();
							}
						} else {
							// only contacts on large cluster level
							if ((*contact_handler)(age1, age2)) {
								p2->StartInfection();
							}
						}
					}
				}
			}
		}
	}


	/// Update the social contacts between the infectious and susceptible members with sorting on health status.
	void Update(unsigned int(Person::*getInternalClusterId)() const, std::shared_ptr<ContactHandler> contact_handler, sort_tag)
	{
		// check if the cluster has infected members and sort (order: non-susceptible, susceptible)
		size_t num_cases = 0;
		bool infectious_cases = false;

		for (size_t i_member = 0; i_member < m_members.size(); i_member++) {
			if (!m_members[i_member]->IsSusceptible()) {
				if(!infectious_cases && m_members[i_member]->IsInfectious()) {
					infectious_cases = true;
				}
				if (i_member > num_cases) {
					std::swap(m_members[i_member], m_members[num_cases]);
				}
				num_cases++;
			}
		}
		if (infectious_cases) {
			// match infectious (in first part of member list) with susceptible members (last part)
			for (size_t i_infected = 0; i_infected < num_cases; i_infected++) {
				const auto p1 = m_members[i_infected];
				if(p1->IsInfectious()) {
					const double age1 = p1->GetAge();
					const size_t sub_cluster1 = (p1->*getInternalClusterId)();
					for (unsigned int i_contact = num_cases; i_contact < m_members.size(); i_contact++) {
						auto p2 = m_members[i_contact];
						const double age2 = p2->GetAge();
						const size_t sub_cluster2 = (p2->*getInternalClusterId)();
						if (sub_cluster1 == sub_cluster2) {
							//contacts on small and large cluster level
							if (contact_handler->makeTwoContacts(age1, age2)) {
								p2->StartInfection();
							}
						} else {
							// only contacts on large cluster level
							if ((*contact_handler)(age1, age2)) {
								p2->StartInfection();
							}
						}
					}
				}
			}
		}
	}

private:
	std::vector<Person*>   m_members;	///< Container with pointers to members of the Cluster.
};


} // namespace flute
} // namespace indismo

#endif // include-guard
