#ifndef FLUTE_POPULATION_H_INCLUDED
#define FLUTE_POPULATION_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header file for the flute Population class
 */

#include <vector>

#include "flute/Person.h"

namespace indismo {
namespace flute {

/**
 * Container with Person objects.
 */
class Population : public std::vector<Person>
{
public:
	/// Add a Person with given data to the Population.
	void AddPerson(
		unsigned int id, double age, unsigned int household_id,
		unsigned int home_district_id, unsigned int day_cluster_id,
		unsigned int day_district_id)
	{
		auto p = Person(id, age, household_id, home_district_id,
				day_cluster_id, day_district_id);
		push_back(p);
	}

	/// Get the cumulative number of cases.
	unsigned int GetInfectedCount() const
	{
		const auto counter  = [](unsigned int total, const Person& p) {
						return total + (p.IsInfected() || p.IsRecovered());
					};
		return std::accumulate(this->begin(), this->end(), 0U, counter);
	}

	/// Get the Population size.
	size_t GetSize() const
	{
		return this->size();
	}

	/// Start an infection of the Person with given index in the container.
	void SetIndexCase(const unsigned int index)
	{
		(*this)[index].SetIndexCase();
	}

};

} // namespace flute
} // namespace indismo

#endif // end of include guard
