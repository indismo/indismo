#ifndef FLUTE_PERSON_H_INCLUDED
#define FLUTE_PERSON_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header file for the Person class.
 */

#include "util/TrackIndexCase.h"

namespace indismo {
namespace flute {

/**
 * Store and handle person data.
 */
class Person
{
public:
	/// Constructor: set the person data.
	Person(unsigned int id, double age, unsigned int household_id, unsigned int home_district_id,
			unsigned int day_cluster_id, unsigned int day_district_id)
		: m_id(id), m_age(age), m_hhId(household_id),
		  m_home_district(home_district_id), m_dayClusterId(day_cluster_id),
		  m_day_district(day_district_id), m_susceptible(true),m_infected(false), m_infectious(false),
		  m_recovered(false), m_disease_counter(0)
	{
	}

	/// Is this person not equal to the given person?
	bool operator!=(const Person& p) const
	{
		return p.m_id != m_id;
	}

	/// Get the id.
	unsigned int GetId() const
	{
		return m_id;
	}

	// Get the age.
	double GetAge() const
	{
		return m_age;
	}

	/// Get the household id.
	unsigned int GetHouseholdId() const
	{
		return m_hhId;
	}

	/// Get the home neighborhood id.
	unsigned int GetHomeDistrictId() const
	{
		return m_home_district;
	}

	/// Get the day cluster id.
	unsigned int GetDayClusterId() const
	{
		return m_dayClusterId;
	}

	/// Get the day neighborhood id.
	unsigned int GetDayDistrictId() const
	{
		return m_day_district;
	}

	/// Is this person susceptible?
	bool IsSusceptible() const
	{
		return m_susceptible;
	}

	/// Is this person infected?
	bool IsInfected() const
	{
		return m_infected;
	}

	/// Is this person infectious?
	bool IsInfectious() const
	{
		return m_infectious;
	}

	/// Is this person recovered?
	bool IsRecovered() const
	{
		return m_recovered;
	}

	/// Get the disease counter.
	unsigned int GetDiseaseCounter() const
	{
		return m_disease_counter;
	}

	/// Increment the persons disease counter.
	void IncrementDiseaseCounter()
	{
		m_disease_counter++;
	}

	/// Reset the persons disease counter.
	void ResetDiseaseCounter()
	{
		m_disease_counter = 0U;
	}

	/**
	 * Set this person as index case.
	 *
	 * @note StartInfection() is not used since this method can be adapted to estimate R0
	 */
	void SetIndexCase()
	{
		m_susceptible 	= false;
		m_infected 	= true;
		ResetDiseaseCounter();
	}

	/**
	 * Start infection.
	 *
	 * @note To estimate R0, we need to track only the index case(s) so secondary cases are not infectious.
	 */
	void StartInfection()
	{
		assert(m_susceptible);
		m_susceptible 	= false;
		m_infected 	= true;
		ResetDiseaseCounter();

		if(TRACK_INDEX_CASE){
			StopInfection();
		}
	}

	/// Stop the infection.
	void StopInfection()
	{
		m_infected   = false;
		m_infectious = false;
		m_recovered  = true;
	}

	/// Update the health status.
	void Update()
	{
		if (IsInfected()) {
			IncrementDiseaseCounter();
			if (GetDiseaseCounter() == 2) {
				m_infectious = true;
			} else if (GetDiseaseCounter() == 6) {
				StopInfection();
			}
		}
	}

private:
	unsigned int    m_id;		   ///< The id.
	double          m_age;		   ///< The age.

	unsigned int    m_hhId;		   ///< The household id.
	unsigned int    m_home_district;   ///< The home district id
	unsigned int    m_dayClusterId;	   ///< The day cluster id
	unsigned int    m_day_district;	   ///< The day district id

	bool		m_susceptible;	   ///< Is this person susceptible?
	bool            m_infected;	   ///< Is this person infected?
	bool            m_infectious;	   ///< Is this person infectious?
	bool            m_recovered;	   ///< Is this person recovered?

	unsigned int    m_disease_counter; ///< The disease counter.
};

} // namespace flute
} // namespace indismo

#endif // end of include guard
