/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Main program of the indismo2 simulator.
 */

#include <cmath>
#include <iostream>
#include <string>
#include <unistd.h>
#include <omp.h>
#include "tclap/CmdLine.h"

#include "sim/Simulator.h"
#include "output/CasesFile.h"
#include "output/SummaryFile.h"
#include "util/Stopwatch.h"
#include "util/TimeStamp.h"

using namespace std;
using namespace std::chrono;
using namespace TCLAP;
using namespace indismo;
using namespace output;
using namespace util;

/// Main program of the indismo2 simulator.
int main(int argc, char** argv)
{
	int exit_status = EXIT_SUCCESS;
	try {
		// -----------------------------------------------------------------------------------------
		// Parse command line and set up task to be executed.
		// -----------------------------------------------------------------------------------------
		CmdLine cmd("indismo", ' ', "1.0", false);

		ValueArg<unsigned int> 	days_Arg(
			"d", "days", "Number of days of the simulation", false, 100, "NUMBER OF DAYS", cmd);
		ValueArg<string> 	model_Arg(
			"m", "model", "Model you want to run", false, "all", "MODEL NAME", cmd);
		ValueArg<string> 	output_prefix_Arg(
			"o", "output_prefix", "The prefix of the output files, default use time stamp",
			 false, TimeStamp().ToTag(), "OUTPUT PREFIX", cmd);
		ValueArg<string> 	population_file_Arg(
			"p", "population_file", "The population file", false,
			"./data/nassau_synt_pop_sorted.csv", "POPULATION CSV FILE", cmd);
		ValueArg<double> 	r0_Arg(
			"r", "r0", "The basic reproduction number", false, 1.1, "BASIC REPRODUCTION NUMBER", cmd);
		ValueArg<unsigned int>  rng_seed_Arg(
			"n", "rng_seed", "Random number generator seed", false, 1U, "SEED", cmd);
		ValueArg<double> 	seeding_rate_Arg(
			"s", "seeding_rate", "Infected people seeding rate", false, 0.00002, "SEEDING RATE", cmd);
		ValueArg<double> 	transm_rate_Arg(
			"t", "transmission_rate", "Transmission Rate", false, -1, "TRANSMISSION RATE", cmd);

		cmd.parse(argc, argv);

		// -----------------------------------------------------------------------------------------
		// Output files
		// -----------------------------------------------------------------------------------------
		SummaryFile  summary_file(output_prefix_Arg.getValue());
		CasesFile    cases_file(output_prefix_Arg.getValue());

		// -----------------------------------------------------------------------------------------
		// Run configurations
		// -----------------------------------------------------------------------------------------
		vector<string> models_to_run;
		if (model_Arg.getValue() == "all") {
			models_to_run = {
				"flute", "flute_sort",
				"fred", "fred_sort",
				"sid", "sid_sort"
			};
		} else {
			models_to_run.push_back(model_Arg.getValue());
		}

		// R0 and transmission rate
		// use Poisson model fitted to simulation data: Expected(R0) = exp(5507*transm_rate + (-0.1911))
		const double r0			= r0_Arg.getValue();
		double transmission_rate 	= transm_rate_Arg.getValue();;
		if(transmission_rate < 0){
			const double b0 = -0.1911;
			const double b1 = 5507;
			transmission_rate = 1/b1*std::log(r0)-b0/b1;
		}

		// other parameters
		const unsigned int num_days     = days_Arg.getValue();
		const string population_file    = population_file_Arg.getValue();
		const unsigned int rng_seed     = rng_seed_Arg.getValue();
		const double seeding_rate  	= seeding_rate_Arg.getValue();

		// -----------------------------------------------------------------------------------------
		// Output to command line.
		// -----------------------------------------------------------------------------------------
		cout << "\n******************************************************" << endl;
		cout << "indismo2 starting up at: " << TimeStamp().ToTag() << endl;
		cout << "Executing: " << argv[0]<< endl;
		cout << "Working directory: " << getcwd(NULL, 0) << endl;
		cout << "Project output tag " << output_prefix_Arg.getValue() << endl;
		cout << "Models to run: ";
		for (const auto& model : models_to_run) {
			cout << model << "; ";
		}
		cout << endl << endl;

		// -----------------------------------------------------------------------------------------
		// Run the simulation for each configuration and output to files.
		// -----------------------------------------------------------------------------------------
		for (const auto& model : models_to_run) {
			// Log
			cout << "Start model : " << model << endl;
			Stopwatch<> total_clock("total_clock", true);

			// Create simulator
			Simulator sim;
			const auto status = sim.Initialize(model, population_file,
				transmission_rate, seeding_rate, rng_seed);
			if (! status) {
				throw invalid_argument("indismo> failed to initialize: " + model);
			}

			// Run simulation
			vector<unsigned int> cases(num_days);
			Stopwatch<> run_clock("run_clock");
			for (unsigned int i = 0; i < num_days; i++) {
				run_clock.Start();
				sim.RunTimeStep();
				run_clock.Stop();
				cases[i] = sim.GetInfectedCount();
			}

			// Output
			summary_file.Print(sim.GetTag(), population_file, num_days,
				sim.GetPopulationSize(), seeding_rate, r0, transmission_rate,
				rng_seed, sim.GetInfectedCount(),
				duration_cast<seconds>(run_clock.Get()).count(),
				duration_cast<seconds>(total_clock.Get()).count());
			cases_file.Print(sim.GetTag(), cases);

			// Log
			cerr << model << "  run_time: " << run_clock.ToString()
				<< "  -- total time: " << total_clock.ToString() << endl << endl;
		}


		// -----------------------------------------------------------------------------------------
		// Done running the simulations ...
		// -----------------------------------------------------------------------------------------
		cout << "indismo exiting at: " << TimeStamp().ToString() << endl << endl;
	}
	catch (exception& e) {
		exit_status = EXIT_FAILURE;
		cerr << e.what() << endl;
	}
	catch(...) {
		exit_status = EXIT_FAILURE;
		cerr << "Unknown exception." << endl;
	}
	return exit_status;
}
