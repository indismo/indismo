#ifndef SIMULATOR_H_INCLUDED
#define SIMULATOR_H_INCLUDED
/*
 *  This file is part of the indismo software.
 *  It is free software: you can redistribute it and/or modify it
 *  under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  any later version.
 *  The software is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with the software. If not, see <http://www.gnu.org/licenses/>.
 *
 *  Reference: Willem L, Stijven S, Tijskens E, Beutels P, Hens N and
 *  Broeckhove J. (2015) Optimizing agent-based transmission models for
 *  infectious diseases, BMC Bioinformatics.
 *
 *  Copyright 2015, Willem L, Stijven S and Broeckhove J.
 */
/**
 * @file
 * Header for the Simulator class.
 */

#include <memory>
#include <string>

namespace indismo {

class IArea;

/**
 * Main class that contains and direct the virtual world.
 */
class Simulator
{
public:
	/// Get the cumulative number of cases.
	unsigned int GetInfectedCount() const;

	/// Get the Populaiton size.
	unsigned int GetPopulationSize() const;

	/// Return a tag that identifies the Simulator implementation.
	std::string GetTag() const;

	/// Initialize the Simulator.
	bool Initialize(const std::string& model, const std::string& population_file_name,
		double transmission_rate, double seeding_rate, unsigned int rng_seed);

	/// Run one time step.
	void RunTimeStep();

private:
	std::shared_ptr<IArea>       m_area;       ///< Points to the Area.
};

} // namespace indismo

#endif // end-of-include-guard
