#!/usr/bin/python

import sys
import os
import argparse
import subprocess
import json
import itertools
import datetime
import shutil



# Function that runs the indismo simulator.
def runIndismo(path, days, rng_seed, seeding_rate, r0, population_file, model, output_prefix, environment={}):
    
    # Check if we need to change the output prefix
    output_pr=[]
    if not output_prefix==None:
        output_pr=['--output_prefix', str(output_prefix)]
    
    # Execute the call
    return subprocess.call([path, '--days', str(days), '--rng_seed', str(rng_seed), '--seeding_rate', str(seeding_rate), '--r0', str(r0), '--population_file', population_file,'--model', model]+output_pr, env=environment)


def main(argv):
    
    # Arguments parser
    parser = argparse.ArgumentParser(description='Script to execute multiple runs of the indismo simulator.')
    parser.add_argument('--config', help='A config file describing the experiments to run.', default='./config/config_default.json', type=str)
    
    args = vars(parser.parse_args())
    
    # Load the json file
    config=json.load(open(args['config'], 'r'))
    
    # Load the experiment configurations from the json
    experiments=[config['threads'], config['rng_seeds'], config['seeding_rates'], config['r0'], config['population_files'], config['model']]
    
    # Check the config'output'... if not empty, use separator in names
    name_sep = ''
    if len(config['output'])>0:
        name_sep='_'
    
    # Create (summary) output dir with dir for experiment output
    time_stamp=datetime.datetime.now().strftime("%m%d%H%M%S") # add '%Y to add year and '%f' for milisec
    file_tag = time_stamp+name_sep+config['output']
    
    output_dir=os.path.join('output',time_stamp+name_sep+config['output'])
    experiments_dirs=os.path.join(output_dir,'experiments')
    if not os.path.isdir(experiments_dirs):
        os.makedirs(experiments_dirs)
    
    # Open the aggregated output files
    output_file=open(os.path.join(output_dir,file_tag+'_output.csv'),'w')
    log_file=open(os.path.join(output_dir,file_tag+'_log.csv'),'w')

    # Copy the json configuration file
    config_file=open(os.path.join(experiments_dirs,'exp_config.json'),'w')
    config_file.write(open(args['config'],'r').read())
    config_file.close()


    # Create a copy of the environment variables to modify and pass to indismo
    env = os.environ.copy()
    
    is_first=True # needed to have only one header in the aggregated output
    
    
    # RUN ALL EXPERIMENTS
    # note: 'intertools.product' makes all combinations within 'experiments'
    # note: (a,b) is a tupule... enumerate(vect) gives ((1, vect_n1) (2,vect_n2) ...)
    for (index, experiment) in enumerate(itertools.product(*experiments)):
        
        # Create a output_prefix for the experiment output
        output_prefix=os.path.join(experiments_dirs,'exp'+str(index))
        
        
        # Set the OpenMP environment
        env['OMP_NUM_THREADS']=str(experiment[0])
        env['OMP_SCHEDULE']=str(config['omp_schedule'])
        
        
        # Run the simulator     ('experiment[0]' has been used for the OMP_NUM_THREADS)
        runIndismo(config['indismo_path'], config['days'], experiment[1], experiment[2], experiment[3], experiment[4], experiment[5], output_prefix,env)
        
        
        # Append the aggregated outputs
        if is_first:
            output_file.write(open(output_prefix+'_output.csv','r').read())
            is_first=False
        else:
            lines = open(output_prefix+'_output.csv','r').readlines()
            for line in lines[1:]:
                output_file.write(line)
        
        log_file.write(open(output_prefix+'_log.csv','r').read())
        log_file.flush()
        output_file.flush()


    output_file.close()
    log_file.close()


if __name__ == "__main__":
    main(sys.argv)